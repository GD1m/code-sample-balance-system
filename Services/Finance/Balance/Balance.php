<?php declare(strict_types=1);

namespace App\Services\Finance;

use App\Events\User\Balance\BalanceTruncated;
use App\Events\User\Balance\MoneyAdded;
use App\Events\User\Balance\MoneySubtracted;
use App\Models\User;
use App\Services\Finance\Transactions\Transactionable;

/**
 * Class Balance
 * @package App\Services\Finance
 */
final class Balance
{
    /**
     * @var User
     */
    private $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param int $amount
     * @param Transactionable|null $basis
     */
    public function addMoney(int $amount, Transactionable $basis = null): void
    {
        event(new MoneyAdded($this->user, $amount, $basis));
    }

    /**
     * @param int $amount
     * @param Transactionable|null $basis
     */
    public function subtractMoney(int $amount, Transactionable $basis = null): void
    {
        event(new MoneySubtracted($this->user, $amount, $basis));
    }

    public function truncateBalance(): void
    {
        event(new BalanceTruncated($this->user->id));
    }
}