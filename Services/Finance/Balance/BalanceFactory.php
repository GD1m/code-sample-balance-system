<?php declare(strict_types=1);

namespace App\Services\Finance\Balance;

use App\Models\User;
use App\Services\Finance\Balance;

/**
 * Class BalanceFactory
 * @package App\Services\Finance\Balance
 */
final class BalanceFactory
{
    /**
     * @param User $user
     * @return Balance
     */
    public function make(User $user): Balance
    {
        return app()->makeWith(Balance::class, compact('user'));
    }
}