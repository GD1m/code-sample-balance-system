<?php declare(strict_types=1);

namespace App\Services\Finance\Money;

/**
 * We store the monetary amount in cents
 *
 * Class MoneyFormat
 * @package App\Helpers
 */
final class MoneyFormat
{
    /**
     * To cents
     *
     * @param $value
     * @return int
     */
    public static function set($value): int
    {
        return $value * 100;
    }

    /**
     * From cents
     *
     * @param int $value
     * @return float
     */
    public static function get(int $value): float
    {
        return round($value !== 0 ? ($value / 100) : 0, 2);
    }

    /**
     * From cents formatted
     *
     * @param int $value
     * @return string
     */
    public static function getFormatted(int $value): string
    {
        return self::formatMoney(self::get($value));
    }

    /**
     * @param float $value
     * @return string
     */
    private static function formatMoney(float $value): string
    {
        if (floor($value) !== $value) {
            return number_format($value, 2, '.', ' ');
        }

        return number_format($value, 0, '.', ' ');
    }


}
