<?php declare(strict_types=1);

namespace App\Services\Finance\Payments;

/**
 * Class Client
 * @package App\Services\Finance\Payments
 */
final class Client
{
    /**
     * @var Provider
     */
    private $provider;

    /**
     * @param Provider $provider
     */
    public function __construct(Provider $provider)
    {
        $this->provider = $provider;
    }

    /**
     * @param Payment $payment
     * @return string
     */
    public function getConfirmationUrl(Payment $payment): string
    {
        return $this->provider->getConfirmationUrl($payment);
    }
}