<?php declare(strict_types=1);

namespace App\Services\Finance\Payments;

use App\Exceptions\InvalidArgumentException;
use App\Http\API\App\V1\Requests\PaymentsHandler\HandlePaymentRequest;
use App\Models\Payment;
use App\Services\Finance\Balance\BalanceFactory;
use App\Services\Finance\Payments\Payment as PaymentContract;
use YandexCheckout\Client;

/**
 * Class HandlePaymentService
 * @package App\Services\Finance\Payments
 */
final class HandlePaymentService
{
    /**
     * @var Client
     */
    private $client;
    /**
     * @var BalanceFactory
     */
    private $balanceFactory;

    /**
     * @param Client $client
     * @param BalanceFactory $balanceFactory
     */
    public function __construct(Client $client, BalanceFactory $balanceFactory)
    {
        $this->client = $client;
        $this->balanceFactory = $balanceFactory;
    }

    /**
     * @param HandlePaymentRequest $request
     * @throws InvalidArgumentException
     * @throws \Throwable
     * @throws \YandexCheckout\Common\Exceptions\ApiException
     * @throws \YandexCheckout\Common\Exceptions\BadApiRequestException
     * @throws \YandexCheckout\Common\Exceptions\ForbiddenException
     * @throws \YandexCheckout\Common\Exceptions\InternalServerError
     * @throws \YandexCheckout\Common\Exceptions\NotFoundException
     * @throws \YandexCheckout\Common\Exceptions\ResponseProcessingException
     * @throws \YandexCheckout\Common\Exceptions\TooManyRequestsException
     * @throws \YandexCheckout\Common\Exceptions\UnauthorizedException
     */
    public function handle(HandlePaymentRequest $request): void
    {
        if ($request->type !== 'notification') {
            throw new InvalidArgumentException(printf('Invalid notification type: %s', $request->type));
        }

        $providerPaymentId = $request->object['id'];

        $payment = $this->getPayment($providerPaymentId);

        $providerPayment = $this->client->getPaymentInfo($providerPaymentId);

        $amount = $providerPayment->getAmount()->getIntegerValue();

        if ($providerPayment->getStatus() !== 'succeeded' || $payment->amount !== $amount) {
            throw new InvalidArgumentException('Invalid request');
        }

        try {
            \DB::beginTransaction();

            $payment->setStatus(PaymentContract::STATUS_CAPTURED);

            $payment->captured_at = now();

            $payment->save();

            $this->balanceFactory
                ->make($payment->user)
                ->addMoney($amount, $payment);

            \DB::commit();
        } catch (\Throwable $exception) {
            \DB::rollBack();

            throw $exception;
        }
    }

    /**
     * @param $providerPaymentId
     * @return PaymentContract
     */
    private function getPayment(string $providerPaymentId): PaymentContract
    {
        return Payment::wherePaymentId($providerPaymentId)->where('status', PaymentContract::STATUS_PENDING)->firstOrFail();
    }
}
