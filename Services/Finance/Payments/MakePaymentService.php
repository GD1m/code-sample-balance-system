<?php declare(strict_types=1);

namespace App\Services\Finance\Payments;

use App\Http\API\App\V1\Requests\Account\Payments\MakePaymentRequest;
use App\Services\Finance\Money\MoneyFormat;
use App\Services\Finance\Payments\Providers\YandexKassa;

/**
 * Class MakePaymentService
 * @package App\Services\Finance\Payments
 */
final class MakePaymentService
{
    /**
     * @var PaymentFactory
     */
    private $paymentFactory;

    /**
     * @param PaymentFactory $paymentFactory
     */
    public function __construct(PaymentFactory $paymentFactory)
    {
        $this->paymentFactory = $paymentFactory;
    }

    /**
     * @param MakePaymentRequest $request
     * @return string
     * @throws \App\Exceptions\InfiniteLoopException
     */
    public function make(MakePaymentRequest $request): string
    {
        $payment = $this->paymentFactory->make($request->user(), MoneyFormat::set((int)$request->amount), $request->returnUrl);

        $client = $this->makeClient(
            app()->make(YandexKassa::class)
        );

        return $client->getConfirmationUrl($payment);
    }

    /**
     * @param Provider $provider
     * @return Client
     */
    private function makeClient(Provider $provider): Client
    {
        return new Client($provider);
    }
}
