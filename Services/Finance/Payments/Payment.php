<?php declare(strict_types=1);

namespace App\Services\Finance\Payments;

/**
 * Interface Payment
 * @package App\Services\Finance\Payments
 */
interface Payment
{
    public const STATUS_UNPAID = 'unpaid';
    public const STATUS_PENDING = 'pending';
    public const STATUS_CAPTURED = 'captured';
    public const STATUS_CANCELED = 'canceled';

    /**
     * @param string $providerPaymentId
     */
    public function setProviderPaymentId(string $providerPaymentId): void;

    /**
     * @param string $status
     */
    public function setStatus(string $status): void;

    public function save();

    /**
     * @return string
     */
    public function getTrackId(): string;

    /**
     * @return int
     */
    public function getAmount(): int;

    /**
     * @return string
     */
    public function getReturnUrl(): string;
}
