<?php declare(strict_types=1);

namespace App\Services\Finance\Payments;

use App\Models\Payment;
use App\Models\User;
use App\Services\Finance\Payments\Payment as PaymentContract;
use App\Services\Security\UuidGenerator;

/**
 * Class PaymentFactory
 * @package App\Services\Finance\Payments
 */
final class PaymentFactory
{
    /**
     * @var UuidGenerator
     */
    private $uuidGenerator;

    /**
     * @param UuidGenerator $uuidGenerator
     */
    public function __construct(UuidGenerator $uuidGenerator)
    {
        $this->uuidGenerator = $uuidGenerator;
    }

    /**
     * @param User $user
     * @param int $amount
     * @param string $returnUrl
     * @return Payment
     * @throws \App\Exceptions\InfiniteLoopException
     */
    public function make(User $user, int $amount, string $returnUrl): Payment
    {
        $payment = new Payment();

        $payment->track_id = $this->uuidGenerator->generate(Payment::class, 'track_id');
        $payment->user_id = $user->id;
        $payment->status = PaymentContract::STATUS_UNPAID;
        $payment->amount = $amount;
        $payment->return_url = $returnUrl;
        $payment->provider = Provider::PROVIDER_YANDEX_KASSA;

        $payment->save();

        return $payment;
    }
}