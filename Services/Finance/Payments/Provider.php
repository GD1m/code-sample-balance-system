<?php declare(strict_types=1);

namespace App\Services\Finance\Payments;

/**
 * Interface Provider
 * @package App\Services\Finance\Payments
 */
interface Provider
{
    public const PROVIDER_YANDEX_KASSA = 'yandex-kassa';

    /**
     * @param Payment $payment
     */
    public function getConfirmationUrl(Payment $payment);
}