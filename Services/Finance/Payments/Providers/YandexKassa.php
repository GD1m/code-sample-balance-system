<?php declare(strict_types=1);

namespace App\Services\Finance\Payments\Providers;

use App\Services\Finance\Money\MoneyFormat;
use App\Services\Finance\Payments\Payment;
use App\Services\Finance\Payments\Provider;
use YandexCheckout\Client;
use YandexCheckout\Model\Confirmation\ConfirmationRedirect;

/**
 * Class YandexKassa
 * @package App\Services\Finance\Payments\Providers
 */
final class YandexKassa implements Provider
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param Payment $payment
     * @return string
     * @throws \Exception
     */
    public function getConfirmationUrl(Payment $payment): string
    {
        $params = [
            'amount' => [
                'value' => $this->amountFormat($payment->getAmount()),
                'currency' => 'RUB',
            ],
            'confirmation' => [
                'type' => 'redirect',
                'return_url' => $payment->getReturnUrl(),
            ],
            'capture' => true,
        ];

        try {
            $response = $this->client->createPayment($params, $payment->getTrackId());
        } catch (\Exception $exception) {
            $payment->setStatus(Payment::STATUS_CANCELED);

            $payment->save();

            throw $exception;
        }

        $status = 'pending' === $response->getStatus()
            ? Payment::STATUS_PENDING
            : Payment::STATUS_CANCELED;

        $payment->setProviderPaymentId($response->getId());
        $payment->setStatus($status);

        $payment->save();

        /** @var ConfirmationRedirect $confirmation */
        $confirmation = $response->getConfirmation();

        return $confirmation->getConfirmationUrl();
    }

    /**
     * @param int $amount
     * @return string
     */
    private function amountFormat(int $amount): string
    {
        return number_format(MoneyFormat::get($amount), 2, '.', '');
    }
}