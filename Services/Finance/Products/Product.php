<?php declare(strict_types=1);

namespace App\Services\Finance\Products;

/**
 * Interface Product
 * @package App\Services\Finance\Products
 */
interface Product
{
    /**
     * @return int
     */
    public function getKey(): int;

    /**
     * @return int
     */
    public function getPrice(): int;

    /**
     * @return string
     */
    public function getForeignKeyName(): string;
}