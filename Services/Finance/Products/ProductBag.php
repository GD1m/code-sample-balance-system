<?php declare(strict_types=1);

namespace App\Services\Finance\Products;

use Illuminate\Support\Collection;

/**
 * Class ProductBag
 * @package App\Services\Finance\Products
 */
final class ProductBag
{
    /**
     * @var Collection
     */
    private $products;

    /**
     * @param Collection $products
     */
    public function __construct(Collection $products)
    {
        $this->products = $products;
    }

    /**
     * @param Product $product
     * @return Collection
     */
    public function push(Product $product): Collection
    {
        if (null === $this->products) {
            $this->products = new Collection();
        }

        return $this->products->push($product);
    }

    /**
     * @return Product|null
     */
    public function pop(): ? Product
    {
        return $this->products->pop();
    }

    /**
     * @return int
     */
    public function getTotalAmount(): int
    {
        return $this->products->sum(function (Product $product) {
            return $product->getPrice();
        });
    }
}