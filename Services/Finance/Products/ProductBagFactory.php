<?php declare(strict_types=1);

namespace App\Services\Finance\Products;

use Illuminate\Support\Collection;

/**
 * Class ProductBagFactory
 * @package App\Services\Finance\Balance
 */
final class ProductBagFactory
{
    /**
     * @param Collection $products
     * @return ProductBag
     */
    public function make(Collection $products): ProductBag
    {
        return app()->makeWith(ProductBag::class, compact('products'));
    }
}