<?php declare(strict_types=1);

namespace App\Services\Finance\Products;

use App\Exceptions\InsufficientFundsException;
use App\Models\Post;
use App\Models\Receipt;
use App\Models\User;
use App\Services\Finance\Balance\BalanceFactory;
use App\Services\Finance\Receipts\ReceiptFactory;

/**
 * Class ProductBuyer
 * @package App\Services\Finance\Products
 */
final class ProductBuyer
{
    /**
     * @var ReceiptFactory
     */
    private $receiptFactory;

    /**
     * @var BalanceFactory
     */
    private $balanceFactory;

    /**
     * @param ReceiptFactory $receiptFactory
     * @param BalanceFactory $balanceFactory
     */
    public function __construct(
        ReceiptFactory $receiptFactory,
        BalanceFactory $balanceFactory
    ) {
        $this->receiptFactory = $receiptFactory;
        $this->balanceFactory = $balanceFactory;
    }

    /**
     * @param User $user
     * @param Post $post
     * @param ProductBag $productBag
     * @throws InsufficientFundsException
     * @throws \App\Exceptions\InfiniteLoopException
     */
    public function tryToBuy(User $user, Post $post, ProductBag $productBag): void
    {
        $this->checkBalance($user, $productBag);

        $this->balanceFactory->make($user)->subtractMoney(
            $productBag->getTotalAmount(),
            $this->makeReceipt($user, $post, $productBag)
        );
    }

    /**
     * @param User $user
     * @param ProductBag $productBag
     * @throws InsufficientFundsException
     */
    private function checkBalance(User $user, ProductBag $productBag): void
    {
        if ($user->balance < $productBag->getTotalAmount()) {
            $message = 'user_id: ' . $user->id
                . ' balance: ' . $user->balance
                . ' products amount: ' . $productBag->getTotalAmount();

            throw new InsufficientFundsException($productBag, $message);
        }
    }

    /**
     * @param User $user
     * @param Post $post
     * @param $productBag
     * @return Receipt
     * @throws \App\Exceptions\InfiniteLoopException
     */
    private function makeReceipt(User $user, Post $post, $productBag): Receipt
    {
        $receipt = $this->receiptFactory->make(
            $user->id, $post->id, $productBag
        );

        return $receipt;
    }
}