<?php declare(strict_types=1);

namespace App\Services\Finance\Receipts;

use App\Models\Receipt;
use App\Services\Finance\Products\ProductBag;
use App\Services\Security\UuidGenerator;

/**
 * Class ReceiptFactory
 * @package App\Services\Finance\Receipts
 */
final class ReceiptFactory
{
    /**
     * @var UuidGenerator
     */
    private $uuidGenerator;

    /**
     * @var ReceiptProductFactory
     */
    private $receiptProductFactory;

    /**
     * @param UuidGenerator $uuidGenerator
     */
    public function __construct(UuidGenerator $uuidGenerator)
    {
        $this->uuidGenerator = $uuidGenerator;
    }

    /**
     * @param int $userId
     * @param int $postId
     * @param ProductBag $products
     * @return Receipt
     * @throws \App\Exceptions\InfiniteLoopException
     */
    public function make(int $userId, int $postId, ProductBag $products): Receipt
    {
        $receipt = new Receipt();

        $receipt->uuid = $this->uuidGenerator->generate(Receipt::class);
        $receipt->user_id = $userId;
        $receipt->post_id = $postId;
        $receipt->amount = $products->getTotalAmount();

        $receipt->save();

        $this->receiptProductFactory = $this->makeReceiptProductFactory($receipt);

        $this->makeProducts($products);

        return $receipt;
    }

    /**
     * @param Receipt $receipt
     * @return ReceiptProductFactory
     */
    private function makeReceiptProductFactory(Receipt $receipt): ReceiptProductFactory
    {
        return app()->makeWith(ReceiptProductFactory::class, [
            'receipt' => $receipt,
        ]);
    }

    /**
     * @param ProductBag $products
     */
    private function makeProducts(ProductBag $products): void
    {
        while ($product = $products->pop()) {
            $this->receiptProductFactory->make($product);
        }
    }
}