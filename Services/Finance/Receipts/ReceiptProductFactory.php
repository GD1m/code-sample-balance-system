<?php declare(strict_types=1);

namespace App\Services\Finance\Receipts;

use App\Models\Receipt;
use App\Models\ReceiptProduct;
use App\Services\Finance\Products\Product;

/**
 * Class ReceiptProductFactory
 * @package App\Services\Finance\Receipts
 */
final class ReceiptProductFactory
{
    /**
     * @var Receipt
     */
    private $receipt;

    /**
     * @param Receipt $receipt
     */
    public function __construct(Receipt $receipt)
    {
        $this->receipt = $receipt;
    }

    /**
     * @param Product $product
     * @return ReceiptProduct
     */
    public function make(Product $product): ReceiptProduct
    {
        $receiptProduct = new ReceiptProduct();

        $receiptProduct->receipt_id = $this->receipt->id;
        $receiptProduct->{$product->getForeignKeyName()} = $product->getKey();
        $receiptProduct->price = $product->getPrice();

        $receiptProduct->save();

        return $receiptProduct;
    }
}
