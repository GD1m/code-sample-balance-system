<?php declare(strict_types=1);

namespace App\Services\Finance\Transactions;

use App\Models\User;
use App\Services\Finance\Balance;

/**
 * Class ReplayTransactionsFactory
 * @package App\Services\Finance\Transactions
 */
final class ReplayTransactionsFactory
{
    /**
     * @param User $user
     * @return ReplayTransactionsService
     */
    public function make(User $user): ReplayTransactionsService
    {
        return app()->makeWith(ReplayTransactionsService::class, [
            'balance' => $this->getBalanceService($user),
        ]);
    }

    /**
     * @param User $user
     * @return Balance
     */
    private function getBalanceService(User $user): Balance
    {
        return app()->makeWith(Balance::class, [
            'user' => $user,
        ]);
    }
}