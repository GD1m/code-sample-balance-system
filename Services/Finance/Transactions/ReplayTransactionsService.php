<?php declare(strict_types=1);

namespace App\Services\Finance\Transactions;

use App\Exceptions\InvalidPropertyException;
use App\Models\Transaction;
use App\Services\Finance\Balance;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class ReplayTransactionsService
 * @package App\Services\Finance\Transactions
 */
final class ReplayTransactionsService
{
    /**
     * @var Balance
     */
    private $balance;

    /**
     * @param Balance $balance
     */
    public function __construct(Balance $balance)
    {
        $this->balance = $balance;
    }

    public function replay(): void
    {
        $this->balance->truncateBalance();

        $this->getUserTransactions()->each(function (Transaction $transaction) {
            $this->replayTransaction($transaction);
        });
    }

    /**
     * @return Transaction[]|Collection
     */
    private function getUserTransactions(): Collection
    {
        $userId = $this->balance->getUser()->id;

        return Transaction::whereUserId($userId)->get();
    }

    /**
     * @param Transaction $transaction
     * @throws InvalidPropertyException
     */
    private function replayTransaction(Transaction $transaction): void
    {
        switch ($transaction->type) {
            case Transaction::TRANSACTION_TYPE_ADD:
                $this->balance->addMoney($transaction->amount, null);
                break;

            case Transaction::TRANSACTION_TYPE_SUBTRACT:
                $this->balance->subtractMoney($transaction->amount, null);
                break;

            default:
                throw new InvalidPropertyException($transaction->type);
        }
    }
}