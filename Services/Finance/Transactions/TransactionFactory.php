<?php declare(strict_types=1);

namespace App\Services\Finance\Transactions;

use App\Models\Transaction;
use App\Services\Security\UuidGenerator;

/**
 * Class TransactionService
 * @package App\Services\Finance\Transactions
 */
final class TransactionFactory
{
    /**
     * @var UuidGenerator
     */
    private $uuidGenerator;

    /**
     * @param UuidGenerator $uuidGenerator
     */
    public function __construct(UuidGenerator $uuidGenerator)
    {
        $this->uuidGenerator = $uuidGenerator;
    }

    /**
     * @param int $userId
     * @param int $amount
     * @param Transactionable $transactionable
     * @return Transaction
     * @throws \App\Exceptions\InfiniteLoopException
     */
    public function make(int $userId, int $amount, Transactionable $transactionable): Transaction
    {
        $transaction = new Transaction();

        $transaction->uuid = $this->uuidGenerator->generate(Transaction::class);
        $transaction->user_id = $userId;
        $transaction->amount = $amount;
        $transaction->type = $transactionable->getTransactionType();

        $transaction->{$transactionable->getForeignKeyName()} = $transactionable->getKey();

        $transaction->save();

        return $transaction;
    }
}