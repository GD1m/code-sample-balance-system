<?php declare(strict_types=1);

namespace App\Services\Finance\Transactions;

/**
 * Interface Transactionable
 * @package App\Services\Finance\Transactions
 */
interface Transactionable
{
    /**
     * @return int
     */
    public function getKey(): int;

    /**
     * @return string
     */
    public function getForeignKeyName(): string;

    /**
     * @return string
     */
    public function getTransactionType(): string;
}