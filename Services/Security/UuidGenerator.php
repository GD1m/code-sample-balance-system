<?php

namespace App\Services\Security;

use App\Exceptions\InfiniteLoopException;
use Ramsey\Uuid\Uuid;

/**
 * Class UuidGenerator
 * @package App\Services\Security
 */
class UuidGenerator
{
    /**
     * @param string $modelClass
     * @param string $fieldName
     * @return string`
     * @throws InfiniteLoopException
     */
    public function generate($modelClass, $fieldName = 'uuid'): string
    {
        $i = 0;
        $maxIterations = 10;

        do {
            $uuid = Uuid::uuid4();
            $i++;
        } while (
            $i <= $maxIterations
            &&
            $modelClass::where($fieldName, $uuid)->exists()
        );

        if ($i === $maxIterations) {
            throw new InfiniteLoopException();
        }

        return $uuid;
    }
}