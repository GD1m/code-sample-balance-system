<?php declare(strict_types=1);

namespace App\Events\User\Balance;

use Illuminate\Foundation\Events\Dispatchable;

/**
 * Class BalanceTruncated
 * @package App\Events\User\Balance
 */
final class BalanceTruncated
{
    use Dispatchable;

    /**
     * @var int
     */
    private $userId;

    /**
     * MoneyAdded constructor.
     * @param int $user_id
     */
    public function __construct(int $user_id)
    {
        $this->userId = $user_id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }
}