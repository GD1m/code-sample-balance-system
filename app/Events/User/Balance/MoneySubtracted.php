<?php declare(strict_types=1);

namespace App\Events\User\Balance;

use App\Models\User;
use App\Services\Finance\Transactions\Transactionable;
use Illuminate\Foundation\Events\Dispatchable;

/**
 * Class MoneySubtracted
 * @package App\Events\User\Balance
 */
final class MoneySubtracted
{
    use Dispatchable;

    /**
     * @var User
     */
    private $user;

    /**
     * @var int
     */
    private $amount;

    /**
     * @var Transactionable|null
     */
    private $basis;

    /**
     * @param User $user
     * @param int $amount
     * @param Transactionable|null $basis
     */
    public function __construct(User $user, int $amount, Transactionable $basis = null)
    {
        $this->user = $user;
        $this->amount = $amount;
        $this->basis = $basis;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @return Transactionable
     */
    public function getBasis(): Transactionable
    {
        return $this->basis;
    }
}