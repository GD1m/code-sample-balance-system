<?php declare(strict_types=1);

namespace App\Exceptions;

use App\Services\Finance\Products\ProductBag;
use Throwable;

/**
 * Class InsufficientFundsException
 * @package App\Exceptions
 */
final class InsufficientFundsException extends \Exception
{
    /**
     * @var ProductBag
     */
    private $productBag;

    /**
     * @param ProductBag $productBag
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(ProductBag $productBag, string $message = '', int $code = 0, Throwable $previous = null)
    {
        $this->productBag = $productBag;

        parent::__construct($message, $code, $previous);
    }

    /**
     * @return ProductBag
     */
    public function productBag(): ProductBag
    {
        return $this->productBag;
    }
}