<?php declare(strict_types=1);

namespace App\Models;

use App\Services\Finance\Transactions\Transactionable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Receipt
 *
 * @package App\Models
 * @property int $id
 * @property string $uuid
 * @mixin \Eloquent
 * @property int $user_id
 * @property int $post_id
 * @property int $amount
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Receipt onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Receipt whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Receipt whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Receipt whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Receipt whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Receipt wherePostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Receipt whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Receipt whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Receipt whereUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Receipt withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Receipt withoutTrashed()
 */
class Receipt extends Model implements Transactionable
{
    use SoftDeletes;

    /**
     * @return int
     */
    public function getKey(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getForeignKeyName(): string
    {
        return 'receipt_id';
    }

    /**
     * @return string
     */
    public function getTransactionType(): string
    {
        return Transaction::TRANSACTION_TYPE_SUBTRACT;
    }
}