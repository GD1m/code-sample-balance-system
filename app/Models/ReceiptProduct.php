<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ReceiptProduct
 *
 * @package App\Models
 * @property int $id
 * @mixin \Eloquent
 * @property int $receipt_id
 * @property int $advance_id
 * @property int $price
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReceiptProduct onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReceiptProduct whereAdvanceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReceiptProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReceiptProduct whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReceiptProduct whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReceiptProduct wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReceiptProduct whereReceiptId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ReceiptProduct whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReceiptProduct withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ReceiptProduct withoutTrashed()
 */
class ReceiptProduct extends Model
{
    use SoftDeletes;
}