<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Transaction
 *
 * @package App\Models
 * @property int $id
 * @property string $uuid
 * @property int $user_id
 * @property int|null $payment_id
 * @property int|null $receipt_id
 * @property string $type
 * @property int $amount
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction wherePaymentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereReceiptId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereUuid($value)
 * @mixin \Eloquent
 */
class Transaction extends Model
{
    public const TRANSACTION_TYPE_ADD = 'add';
    public const TRANSACTION_TYPE_SUBTRACT = 'subtract';

    public const TRANSACTION_TYPES = [
        self::TRANSACTION_TYPE_ADD,
        self::TRANSACTION_TYPE_SUBTRACT,
    ];
}