<?php

namespace App\Models;

use App\Services\Finance\Money\MoneyFormat;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string|null $email
 * @property string|null $phone
 * @property string $password
 * @property string|null $first_name
 * @property string|null $last_name
 * @property int $balance
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @mixin \Eloquent
 */
class User extends \Illuminate\Database\Eloquent\Model
{
    /**
     * @return string
     */
    public function formattedBalance(): string
    {
        return MoneyFormat::getFormatted($this->balance);
    }
}